var http = require('http')
var fs = require('fs')
var url = require("url")
var mime = require('mime')

var server = http.createServer(function (request, response) {
  var pathname = url.parse(request.url).pathname

  if(pathname == "/") {
      var editorHtml = "./dist/index.html"
      fs.readFile(editorHtml, (err,data) => {
        if (err) { 
          response.writeHead(404)
          return response.end()
        }
        response.setHeader('content-type', mime.getType(editorHtml))
        response.writeHead(200)
        response.write(data);
        response.end()
      })
  } else {
      fs.readFile(`./dist/${pathname}`, (err,data) => {
        if (err) { 
          response.writeHead(404)
          return response.end()
        }
        response.setHeader('content-type', mime.getType(`./dist/${pathname}`))
        response.writeHead(200)
        response.write(data)
        response.end()
      })
      
  }

})

server.listen(5050);
 
console.log('Server Started listening on 5050')