const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const clientConfig = {
  entry: './src/editor/js/editor.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js',
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {from:'./src/editor/html/editor.html',to:'./index.html'},
        {from:'./src/editor/css',to:'./css'},
        {from:'./src/editor/images',to:'./images'},
        {from:'./src/vendor',to:'./vendor'},
        {from:'./package.json',to:'./package.json'},
        {from:'./favicon.ico',to:'./favicon.ico'} 
      ]
    }),
  ]
}
module.exports = [clientConfig];