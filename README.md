# Tischtennis Mannschafts Planer Web Application

Autor: Dominik Lüke  

---

Der **Tischtennis Mannschafts Planer** ist ein freies und quelloffenes Tool, um Aufstellungen für Tischtennis Mannschaften zu erstellen.

Dieses Tool bietet eine Unterstützung zur Erstellung von WTTV-Wettspielordnungs-konformen Aufstellungen in Bezug auf Spielstärke-Reihenfolge nach QTTR-Werten und Mannschafts-Sollstärken [Stand 01/2020](assets/WO2020-01-01.pdf), Abschnitt H).

Es garantiert allerdings nicht, dass die mit diesem Tool erstellten Aufstellungen vollständig WO-konform sind. Bitte überprüfe die erstellten Aufstellung stets noch einmal auf Richtigkeit. Einige Sonderstatus wie z.B. Jugend-Ersatzspieler oder gleichgestellte Ausländer werden nicht berücksichtigt.

Die Aufstellungen vergangener Halbserien, TTR-Werte und Bilanzen können manuell von den [myTischtennis.de](https://mytischtennis.de) Seiten heruntergeladen werden. Für die eingebetteten Inhalte von myTischtennis.de wird keine Haftung übernommen. Für einige Funktionen ist ein Account bei myTischtennis.de notwendig.

---

Built with
[Bootstrap](https://getbootstrap.com/docs/4.4/getting-started/introduction/),
[Font Awesome](https://fontawesome.com/v4.7.0/icons/),
[jQuery](https://jquery.com/),
[jQueryUI,](https://jqueryui.com/),
[popper.js](https://popper.js.org/),
[dropzone.js](https://docs.dropzone.dev/getting-started/installation/stand-alone)

## Hilfe

[Zur Hilfe-Seite](assets/help/HELP.md)

## License

[CC0 1.0 (Public Domain)](LICENSE.md)

## Für Entwickler

### Selber bauen

Klone diese Repository:

```powershell
git clone git@bitbucket.org:dominiklueke/ttr-mannschafts-planer-web.git
```

Mit [node.js](https://nodejs.org) starten:

```powershell
## Install node packages
npm install

## Build
npx webpack

## Start
npm start
```
