/* DEFINE GLOBAL CONSTANTS HERE */

export const SPIELKLASSEN = {
  Herren: {
    "Herren":{
      name: "Herren",
      url_slug: "H"
    }
  },
  Damen: {
    "Damen":{
      name: "Damen",
      url_slug: "D"
    }
  },
  Mädchen: {
    "Mädchen 19":{
      name: "Mädchen 19",
      url_slug: "M19"
    },
    "Mädchen 18":{
      name: "Mädchen 18",
      url_slug: "M18"
    },
    "Mädchen 15":{
      name: "Mädchen 15",
      url_slug: "M15"
    },
    "Mädchen 13":{
      name: "Mädchen 13",
      url_slug: "M13"
    },
    "Mädchen 11":{
      name: "Mädchen 11",
      url_slug: "M11"
    }
  },
  Jungen: {
    "Jungen 19":{
      name: "Jungen 19",
      url_slug: "J19"
    },
    "Jungen 18":{
      name: "Jungen 18",
      url_slug: "J18"
    },
    "Jungen 15":{
      name: "Jungen 15",
      url_slug: "J15"
    },
    "Jungen 13":{
      name: "Jungen 13",
      url_slug: "J13"
    },
    "Jungen 11":{
      name: "Jungen 11",
      url_slug: "J11"
    }
  },
  Seniorinnen: {
    "Seniorinnen 40":{
      name: "Seniorinnen 40",
      url_slug: "wS40"
    },
    "Seniorinnen 50":{
      name: "Seniorinnen 50",
      url_slug: "wS50"
    },
    "Seniorinnen 60":{
      name: "Seniorinnen 60",
      url_slug: "wS60"
    },
    "Seniorinnen 70":{
      name: "Seniorinnen 70",
      url_slug: "wS70"
    }
  },
  Senioren: {
    "Senioren 40":{
      name: "Senioren 40",
      url_slug: "mS40"
    },
    "Senioren 50":{
      name: "Senioren 50",
      url_slug: "mS50"
    },
    "Senioren 60":{
      name: "Senioren 60",
      url_slug: "mS60"
    },
    "Senioren 70":{
      name: "Senioren 70",
      url_slug: "mS70"
    },
  }
}
export const JUGEND_SPIELKLASSEN = ["Mädchen","Jungen"]

export const SOLLSTAERKEN = {
  Herren: 6,
  Damen: 4,
  Mädchen: 4,
  Jungen: 4,
  Seniorinnen: 4,
  Senioren: 4,
}

export const VERFUEGBARKEITEN_LABEL = {
  100: "Stammspieler",
  75: '"Fehlt öfters mal"',
  50: '"Halbe Kraft"',
  25: "Ersatzspieler",
  0: "Spielt selten oder nie",
}

export const VERBÄNDE = {
  BaTTV: "Baden",
  ByTTV: "Bayern",
  TTVB: "Brandenburg",
  FTTB: "Bremen",
  HaTTV: "Hamburg",
  HeTTV: "Hessen",
  TTVMV: "Mecklenburg-Vorpommern",
  TTVN: "Niedersachsen",
  PTTV: "Pfalz",
  RTTVR: "Rheinland Rheinhessen",
  STTB: "Saarland",
  TTVSA: "Sachsen-Anhalt",
  TTTV: "Thüringen",
  WTTV: "NRW",
  TTBW: "Baden Württemberg"
}
export const DEFAULT_VERBAND = "WTTV"

export const VORRUNDEN_MONATE = [8,9,10,11,12]

export const TTR_TOLERANZ = 50
export const TTR_TOLERANZ_INTERN = 35
export const TTR_TOLERANZ_JUGEND_BONUS = 35
export const TTR_TOLERANZ_DKADER_BONUS = 35

export function GET_URL_SLUG_OF_VEREIN(verein="") {
  return verein.replace(/ /g,"-").replace(/ä/g,"ae").replace(/ö/g,"oe").replace(/ü/g,"ue").replace(/\./g,"-").replace(/\//g,"-")
}

export function COMPARE_HALBSERIEN(a,b) {
  // Sort halbserien descending
  // valid inputs: for a and b [Vorrunde|Rückrunde][ |-]d+[/]d*
  var a_split = " "
  if (a.includes("-")) { a_split = "-" }
  var b_split = " "
  if (b.includes("-")) { b_split = "-" }
  var a_sort_halbserie = a.split(a_split)[0]
  var a_sort_saison = parseInt(a.split(a_split)[1].replace("/",0), 10)
  var b_sort_halbserie = b.split(b_split)[0]
  var b_sort_saison = parseInt(b.split(b_split)[1].replace("/",0), 10)
  if ( (a_sort_saison - b_sort_saison) === 0 ){
    return a_sort_halbserie.localeCompare(b_sort_halbserie) // Vorrunde > Rückrunde
  } else {
    return b_sort_saison - a_sort_saison
  }
}

export function GET_OTHER_HALBSERIE(current_halbserie){
  return current_halbserie== "Vorrunde" ? "Rückrunde" : "Vorrunde"
}

export function GET_PREVIOUS_SAISON(current_halbserie, current_saison){
  var prevSaisonRaw = `${parseInt( current_saison.replace("/",""),10) - 101}`
  return current_halbserie == "Vorrunde" ? [prevSaisonRaw.slice(0,4),"/",prevSaisonRaw.slice(4)].join('') : current_saison
}

export function GET_NEXT_SAISON(current_halbserie, current_saison){
  var nextSaisonRaw = `${parseInt( current_saison.replace("/",""),10) + 101}`
  return current_halbserie == "Vorrunde" ? [nextSaisonRaw.slice(0,4),"/",nextSaisonRaw.slice(4)].join('') : saison
}
