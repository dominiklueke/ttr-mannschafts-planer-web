// model
import Model from './../model/model'
// parser
import MyTTAufstellungsParser from './../parser/myTTAufstellungsParser'
import MyTTBilanzenParser from './../parser/myTTBilanzenParser'
import MyTTRWerteParser from './../parser/myTTRWerteParser'
// views
// view alert
import AlertView from './../view/alert/alertView'
// view editor
import EditorView from './../view/editor/editorView'
// view footer
import FooterView from './../view/footer/footerView'
// view header
import MenuView from './../view/header/menuView'
import HeaderView from './../view/header/headerView'
// view modals
import AboutModalView from './../view/modals/aboutModal/aboutModalView'
import MyTTModalView from './../view/modals/myTTModal/myTTModalView'
import NewPlanungModalView from './../view/modals/newPlanung/newPlanungModalView'
import PlanungTagsModalView from './../view/modals/planungTags/planungTagsModalView'
import WelcomeModalView from './../view/modals/welcomeModal/welcomeModalView'
// view progressbar
import ProgressBarView from './../view/progessBar/progressBarView'
// view sidebar
import SidebarView from './../view/sidebar/sidebarView'


export default class Controller {

  constructor() {
    // PROGRESSBAR VIEW (start loading...)
    this.progessBarView = new ProgressBarView()
    this.showProgressBar("primary","white","",true,1000)

    /* MODEL */
    this._createModel()

    /* VIEW */
    // HEADER
    this._createHeaderView()
    // EDITOR
    this._createEditorView()
    // FOOTER
    this._createFooterView()
    // SIDEBAR
    this._createSidebarView()
    // MYTTMODAL
    this._createMyTTModalView()
    // NEWPLANUNGSMODAL
    this._createNewPlanungModalView()
    // PLANUNGTAGSMODAL
    this._createPlanungTagsModalView()
    // ABOUTMODAL
    this._createAboutModalView()
    // WELCOME MODAL
    this.welcomeModalView = new WelcomeModalView()
    // ALERT
    this.alertView = new AlertView()

    // Initial Display
    this.displayWelcomeModal(false)
    this.updateView()
  }

  /* CONSTRUCTORS */
  _createModel = () => {
    this.model = new Model()
    // Parser
    this.myTTAufstellungsParser = new MyTTAufstellungsParser()
    this.myTTBilanzenParser = new MyTTBilanzenParser()
    this.myTTRWerteParser = new MyTTRWerteParser()
    // Bind Model Handlers
    this.model.bindSidebarViewChanged(this.onSidebarViewChanged)
    this.model.bindFooterDataChanged(this.onFooterDataChanged)
    this.model.bindFileSavedChanged(this.onFileSavedChanged)
    this.model.planung.bindMannschaftenChanged(this.onMannschaftenChanged)
    this.model.planung.bindHeaderDataChanged(this.onHeaderDataChanged)
    this.model.planung.bindErrorOccured(this.alertError)
  }

  _createHeaderView = () => {
    this.headerView = new HeaderView()
    this.headerView.bindClickOnReloadDataButon(this.handleClickOnReloadDataButton)
    // create menu after the header is created
    this.menuView = new MenuView()
    this.menuView.bindClickOnNewPlanungButton(this.handleClickOnNewPlanungButton)
    this.menuView.bindClickOnOpenButton(this.handleClickOnOpenButton)
    this.menuView.bindClickOnSaveButton(this.handleClickOnSaveButton)
    this.menuView.bindClickOnPrintButton(window.print)
    this.menuView.bindClickOnAboutButton(this.displayAboutModal)
    this.menuView.bindClickOnWelcomeButton(this.displayWelcomeModal)
    this.menuView.bindClickOnCloseButton(this.handleClickOnCloseButton)
  }

  _createEditorView = () => {
    this.editorView = new EditorView()
    this.editorView.bindClickOnNeuePlanungButton(this.createNewPlanung)
    this.editorView.bindClickOnOeffnePlanungButton(this.handleClickOnOpenButton)
    this.editorView.bindSpielklasseExpanded(this.handleExpandSpielklasse)
  }

  _createFooterView = () => {
    this.footerView = new FooterView()
    this.footerView.bindAddTagToPlanung(this.handleAddTagToPlanung)
    this.footerView.bindClickOnViewSwitchButton(this.handleViewSwitch)
    this.footerView.bindClickOnUndoButton(this.undo)
    this.footerView.bindClickOnRedoButton(this.redo)
    this.footerView.bindLoadTag(this.handleClickLoadTag)
  }

  _createSidebarView = () => {
    this.sidebarView = new SidebarView()
    // Handler SIDEBAR VIEW
    this.sidebarView.bindClickCloseButtonOnSidebar(this.handleClickCloseButtonOnSidebar)
    // Handler SIDEBAR SPIELER VIEW
    this.sidebarView.bindEditNameOnSpieler(this.handleEditNameOnSpieler)
    this.sidebarView.bindEditQttrOnSpieler(this.handleEditQttrOnSpieler)
    this.sidebarView.bindEditJahrgangOnSpieler(this.handleEditGeburstdatumOnSpieler)
    this.sidebarView.bindClickResButtonOnSpieler(this.handleClickResButtonOnSpieler)
    this.sidebarView.bindClickSbeButtonOnSpieler(this.handleClickSbeButtonOnSpieler)
    this.sidebarView.bindClickMannschaftButtonOnSpieler(this.handleClickMannschaftButtonOnSpieler)
    this.sidebarView.bindChangeVerfuegbarkeitOnSpieler(this.handleChangeVerfuegbarkeitOnSpieler)
    this.sidebarView.bindClickFarbeButtonOnSpieler(this.handleClickFarbeButtonOnSpieler)
    this.sidebarView.bindEditKommentarOnSpieler(this.handleEditKommentarOnSpieler)
    this.sidebarView.bindClickDeleteButtonOnSpieler(this.handleClickDeleteButtonOnSpieler)
    // Handler SIDEBAR MANNSCHAFT VIEW
    this.sidebarView.bindEditLigaOnMannschaft(this.handleEditLigaOnMannschaft)
    this.sidebarView.bindEditSollstaerkeOnMannschaft(this.handleEditSollstaerkeOnMannschaft)
    this.sidebarView.bindEditSpieltagOnMannschaft(this.handleEditSpieltagOnMannschaft)
    this.sidebarView.bindEditUhrzeitOnMannschaft(this.handleEditUhrzeitOnMannschaft)
    this.sidebarView.bindEditSpielwocheOnMannschaft(this.handleEditSpielwocheOnMannschaft)
    this.sidebarView.bindEditKommentarOnMannschaft(this.handleEditKommentarOnMannschaft)
    this.sidebarView.bindClickDeleteButtonOnMannschaft(this.handleClickDeleteButtonOnMannschaft)
  }

  _createMyTTModalView = () => {
    this.myTTModalView = new MyTTModalView()
    this.myTTModalView.bindHtmlPreParser("aufstellung", this.preParseAufstellungHtml)
    this.myTTModalView.bindHtmlParser("aufstellung", this.parseAufstellung)
    this.myTTModalView.bindParseResultAnalyzer("aufstellung", this.getAufstellungsParseResult)
    this.myTTModalView.bindClickOnLoadButtonOnMyTTModalTab("aufstellung", this.handleClickAufstellungLadenButtonOnMyTTModal)

    this.myTTModalView.bindHtmlPreParser("ttrwerte", this.preParseTtrRanglisteHtml)
    this.myTTModalView.bindHtmlParser("ttrwerte", this.parseTtrRangliste)
    this.myTTModalView.bindParseResultAnalyzer("ttrwerte", this.getTtrRanglisteParseResult)
    this.myTTModalView.bindClickOnLoadButtonOnMyTTModalTab("ttrwerte", this.handleClickTTRWerteLadenButtonOnMyTTModal)

    this.myTTModalView.bindHtmlPreParser("bilanzen", this.preParseBilanzenHtml)
    this.myTTModalView.bindHtmlParser("bilanzen", this.parseBilanzen)
    this.myTTModalView.bindParseResultAnalyzer("bilanzen", this.getBilanzenParseResult)
    this.myTTModalView.bindClickOnLoadButtonOnMyTTModalTab("bilanzen", this.handleClickBilanzenLadenButtonOnMyTTModal)
  }

  _createNewPlanungModalView = () => {
    this.newPlanungModalView = new NewPlanungModalView()
    this.newPlanungModalView.bindClickSubmitPlanungButton(this.handleClickSubmitPlanungButton)
  }

  _createPlanungTagsModalView = () => {
    this.planungTagsModalView = new PlanungTagsModalView()
    this.planungTagsModalView.bindLoadTag(this.handleClickLoadTag)
    this.planungTagsModalView.bindDeleteTag(this.handleClickDeleteTag)
  }

  _createAboutModalView = () => {
    this.aboutModalView = new AboutModalView()
  }

  /* VIEW SWITCH */
  handleViewSwitch = (meldung) => {
    if(meldung){
      this.onMannschaftenChanged()
    } else {
      this.sidebarView.hideSidebar()
      this.editorView.displayMannschaften(this.model, false)
    }
    
  }

  /* UNDO + REDO */

  undo = () => {
    this.model.undo()
  }

  redo = () => {
    this.model.redo()
  }

  /* ABOUT MODAL */

  displayAboutModal = () => {
    this.aboutModalView.displayAboutModal()
  }

  /* WELCOME MODAL */
  displayWelcomeModal = (force) => {
    if (force) {
      localStorage.removeItem('skipWelcomeScreen')
    }
    this.welcomeModalView.displayWelcomeModal()
  }
  

  /* PLANUNG */

  createNewPlanung = () => {
    // close current planung save
    this.closePlanungSave().then((result) => {
      if (result) {
        // Show Planungs Modal
        this.newPlanungModalView.displayPlanungModal()
      }
    })
  }

  download = (filename, text) => {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  saveFile = () => {
    const planung_json = JSON.parse(this.getPlanungAsJsonString())
    // set saved to true in the planung in the file
    planung_json.saved = true
    // store tags as part of the planung for legacy reasons 
    // (versions prior to 2.2 need a file format where the planung is plain inside the file)
    planung_json.tags = JSON.stringify(this.model.tags)
    const file_content_str = JSON.stringify(planung_json)
    const friendly_saison = planung_json.saison.replace("/","")
    const filename_suggestion = `Saisonplanung-${planung_json.verein}-${planung_json.spielklasse}-${planung_json.halbserie}-${friendly_saison}.ttsp`
    this.download(filename_suggestion, file_content_str)
    this.model.setSaved(true)
    return true
  }

  confirmClosePlanungDialog = () => {
    return window.confirm("Ungespeicherte Änderungen\n\nMöchten Sie die Planung schließen, ohne ihre Änderungen zu speichen?\n\nIhre Änderungen gehen verloren, wenn Sie diese nicht speichern.")
  }
  
  closePlanungSave = () => {
    if ( ! this.model.saved ) {
      // show confirm dialog if current planung is unsafed
      let confirmclosedialogresult = this.confirmClosePlanungDialog()
      if (confirmclosedialogresult) {
        return new Promise((resolve, reject) => {
          this.closePlanung()
          resolve(true)
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve(false)
        })
      }
    }
    return new Promise((resolve, reject) => {
      this.closePlanung()
      resolve(true)
    })
  }

  closePlanung = () => {
    // reset local storage
    localStorage.removeItem('localStorageFilepath')
    localStorage.removeItem('localStorageFilepathQuit')
    localStorage.removeItem('localStoragePlanung')
    localStorage.removeItem('localStoragePlanungTags')
    localStorage.setItem('localStorageFileSaved',"true")
    // New planung
    this.model = new Model()
    // Bind Handlers
    this.model.bindSidebarViewChanged(this.onSidebarViewChanged)
    this.model.bindFooterDataChanged(this.onFooterDataChanged)
    this.model.bindFileSavedChanged(this.onFileSavedChanged)
    this.model.planung.bindMannschaftenChanged(this.onMannschaftenChanged)
    this.model.planung.bindHeaderDataChanged(this.onHeaderDataChanged)
    this.model.planung.bindErrorOccured(this.alertError)
    // reset and hide Planungs Modal
    this.newPlanungModalView.destroyPlanungModal()
    this._createNewPlanungModalView()
    // reset and hide MyTT Modal
    this.myTTModalView.destroyMyTTModal()
    this._createMyTTModalView()
    // reset Editor View
    this.editorView.destroy()
    this._createEditorView()
    // Initial Display
    this.updateView()
  }

  getPlanungAsJsonString = () => {
    return this.model.planung.getPlanungAsJsonString()
  }

  openPlanung = (file) => {
    this.showProgressBar("primary","white","",true) // start "loading"
    const reader = new FileReader();
    reader.addEventListener('load', (event) => {
      try {
        const file_content_str = event.target.result;
        const file_content = JSON.parse(file_content_str)
        // set tags
        if (file_content.hasOwnProperty('tags')){
          this.model.setTags(file_content.tags)
        }
        // update planung
        this.model.updatePlanung(file_content, true)
        this.setPlanungFile(file.name)
        this.model.setSaved(true)
      } catch (e) {
        this.alertError(`An error ocurred reading the file ${file.name}:<br/>${e}`)
      }
      // hide progress bar
      this.hideProgressBar()
    });
    reader.readAsText(file)
  }

  setPlanungFile = (filepath) => {
    this.model.setFile(filepath)
    localStorage.setItem('localStorageFilepath', filepath)
  }

  _updateDocumentTitle = () => {
    const app_title = "Tischtennis Mannschafts Planer"
    $(document).attr("title", `${app_title}`);
  }

  /* UPDATE */

  updateView = () => {
    this.onHeaderDataChanged(this.model.planung)
    this.onMannschaftenChanged()
    this.onFooterDataChanged(this.model)
    this._updateDocumentTitle()
  }

  onHeaderDataChanged = (planung) => {
    this.headerView.updateHeader(planung)
    this.myTTModalView.setHomeUrl("aufstellung", planung.aufstellung.url)
    this.myTTModalView.setHomeUrl("ttrwerte", planung.ttrwerte.url)
    this.myTTModalView.setHomeUrl("bilanzen", planung.bilanzen.url)
  }

  onMannschaftenChanged = () => {
    this.editorView.displayMannschaften(this.model, true)
    this.editorView.bindAddMannschaft(this.handleAddMannschaft)
    this.editorView.bindClickOnLadeAufstellungLink(this.handleClickOnLadeAufstellungLink)
    this.editorView.bindClickOnMannschaft(this.handleClickOnMannschaft)
    this.editorView.bindAddSpieler(this.handleAddSpieler)
    this.editorView.bindClickOnSpieler(this.handleClickOnSpieler)
    this.editorView.bindToggleSpvOnSpieler(this.handleToggleSpvOnSpieler)
    this.editorView.bindReorderSpieler(this.handleReorderSpieler)
    this.editorView.bindReorderMannschaft(this.handleReorderMannschaft)
    this.editorView.bindSpielklasseExpanded(this.handleExpandSpielklasse)
    this.myTTModalView.notifyPlanungUpdated()
    this.onSidebarViewChanged()
  }

  onSidebarViewChanged = () => {
    const display = this.model.view.sidebar.display 
    const id = this.model.view.sidebar.id
    if (display == "spieler") {
      const spieler = this.model.planung.spieler.getSpieler(id)
      if (spieler) {
        this.sidebarView.displaySpieler(spieler)
        this.editorView.focusSpieler(spieler)
      }
    } else if (display == "mannschaft") {
      const mannschaft = this.model.planung.mannschaften.getMannschaft(id)
      if (mannschaft){
        this.sidebarView.displayMannschaft(mannschaft)
        this.editorView.focusMannschaft(mannschaft)
      }
    } else {
      this.sidebarView.hideSidebar()
      this.editorView.removeFocus()
    }
  }

  onFooterDataChanged = (model) => {
    this.footerView.update(model)
    this.planungTagsModalView.update(model)
  }

  onFileSavedChanged = () => {
    this._updateDocumentTitle()
  }

  alertError = (error="A internal Error occured") => {
    this.alert("danger", error, -1)
  }

  alert = (type="primary", html_content="", timeout=3000) => {
    this.alertView.displayAlert(type, html_content, timeout)
  }

  showProgressBar = (color="primary", textcolor="white", text="", fullscreen=false, timeout=-1) => {
    this.progessBarView.show(color, textcolor, text, fullscreen, timeout)
  }

  hideProgressBar = () => {
    this.progessBarView.hide()
  }

  /* HEADER HANDLER */

  handleClickOnNewPlanungButton = () => {
    this.createNewPlanung()
  }

  handleClickOnSaveButton = () => {
    this.saveFile()
  }

  handleClickOnOpenButton = (filelist) => {
    this.closePlanungSave().then((result) => {
      if ( result ) {
        this.openPlanung(filelist[0])
      }
    })
  }

  handleClickOnCloseButton = () => {
    this.createNewPlanung()
  }

  handleClickOnReloadDataButton = (tab) => {
    this.myTTModalView.showTab(tab)
  }

  /* NEW PLANUNG MODAL HANDLER */
  handleClickSubmitPlanungButton = (planung_json) => {
    this.model.resetView()
    this.model.updatePlanung(planung_json, false)
    //ipcRenderer.send('enableSaveExportPrintClose')
  }

  parseAufstellungUrl = (url) => {
    return this.myTTAufstellungsParser.parseUrl(url)
  }

  /* MYTT MODAL WEBVIEW HANDLER */

  preParseAufstellungHtml = (html) => {
    return this.myTTAufstellungsParser.preParseHtml(html)
  }

  preParseTtrRanglisteHtml = (html) => {
    return this.myTTRWerteParser.preParseHtml(html)
  }

  preParseBilanzenHtml = (html) => {
    return this.myTTBilanzenParser.preParseHtml(html)
  }

  parseAufstellung = (url, html) => {
    return this.myTTAufstellungsParser.parse(url, html)
  }

  parseTtrRangliste = (url, html) => {
    return this.myTTRWerteParser.parse(url, html)
  }

  parseBilanzen = (url, html) => {
    return this.myTTBilanzenParser.parse(url, html)
  }

  getAufstellungsParseResult = (planung) => {
    return this.myTTAufstellungsParser.getResult(planung, this.model.planung)
  }

  getTtrRanglisteParseResult = (planung) => {
    return this.myTTRWerteParser.getResult(planung, this.model.planung)
  }

  getBilanzenParseResult = (planung) => {
    return this.myTTBilanzenParser.getResult(planung, this.model.planung)
  }

  handleClickAufstellungLadenButtonOnMyTTModal = (planung_json) => {
    // Hide the sidebar as the currently shown spieler/mannschaft might not be there after load
    this.model.closeSidebar()
    // load the aufstellung 
    this.model.updateAufstellungFromMyTT(planung_json)
  }

  handleClickTTRWerteLadenButtonOnMyTTModal = (planung_json) => {
    // Load TTR Werte
    this.model.updateTTRWerteFromMyTT(planung_json)
  }

  handleClickBilanzenLadenButtonOnMyTTModal = (planung_json) => {
    // Load Bilanzen
    this.model.updateBilanzenFromMyTT(planung_json)
  }

  /* EDITOR HANDLER */

  handleExpandSpielklasse(model, spielklasse, expanded){
    model.expandSpielklasse(spielklasse, expanded)
  }

  handleClickOnLadeAufstellungLink = () => {
    this.myTTModalView.showTab('aufstellung')
  }

  handleAddMannschaft = (spielklasse, nummer) => {
    this.model.addMannschaft(spielklasse, nummer)
  }

  handleAddSpieler = (spielklasse, mannschaft, position, name, qttr) => {
    this.model.addSpieler(spielklasse, mannschaft, position, name, qttr)
  }

  handleReorderSpieler = (id, spielklasse, new_mannschaft, new_position) => {
    this.model.planung.reorderSpieler(id, spielklasse, new_mannschaft, new_position)
  }

  handleReorderMannschaft = (old_spielklasse, new_spielklasse, old_mannschaft, new_mannschaft) => {
    this.model.planung.reorderMannschaft(old_spielklasse, new_spielklasse, old_mannschaft, new_mannschaft)
  }

  handleClickOnSpieler = (id) => {
    this.model.displaySpielerDetails(id)
  }

  handleClickOnMannschaft = (id) => {
    this.model.displayMannschaftDetails(id)
  }

  handleToggleSpvOnSpieler = (id, spv) => {
    this.model.planung.editSpielerSpv(id, spv)
  }

  /* FOOTER HANDLER */

  handleAddTagToPlanung = (tag) => {
    this.model.addTagToPlanung(tag)
  }

  /* PLANUNG TAG MODAL HANDLER */
  handleClickLoadTag = (tag_id) => {
    this.model.loadTag(tag_id)
  }
  
  handleClickDeleteTag = (tag_id) => {
    this.model.deleteTag(tag_id)
  }

  /* SIDEBAR HANDLER */

  handleClickCloseButtonOnSidebar = () => {
    this.model.closeSidebar()
  }

  /* SIDEBAR SPIELER HANDLER */

  handleEditNameOnSpieler = (id, name) => {
    this.model.planung.editSpielerName(id, name)
  }

  handleEditQttrOnSpieler = (id, qttr) => {
    this.model.planung.editSpielerQttr(id, qttr)
  }

  handleEditGeburstdatumOnSpieler = (id, jahrgang) => {
    this.model.planung.editSpielerJahrgang(id, jahrgang)
  }

  handleClickResButtonOnSpieler = (id, res) => {
    this.model.planung.editSpielerRes(id, res)
  }

  handleClickSbeButtonOnSpieler = (id, sbe) => {
    this.model.planung.editSpielerSbe(id, sbe)
  }

  handleClickMannschaftButtonOnSpieler = (id, mannschaft, value) => {
    this.model.planung.editSpielerSpieltInMannschaft(id, mannschaft, value)
  }

  handleChangeVerfuegbarkeitOnSpieler = (id, verfuegbarkeit) => {
    this.model.planung.editSpielerVerfuegbarkeit(id, verfuegbarkeit)
  }

  handleClickFarbeButtonOnSpieler = (id, farbe) => {
    this.model.planung.editSpielerFarbe(id, farbe)
  }

  handleEditKommentarOnSpieler = (id, kommentar) => {
    this.model.planung.editSpielerKommentar(id, kommentar)
  }

  handleClickDeleteButtonOnSpieler = (id) => {
    this.model.deleteSpieler(id)
  }

  /* SIDEBAR MANNSCHAFT HANDLER */

  handleEditLigaOnMannschaft = (id, liga) => {
    this.model.planung.editMannschaftLiga(id, liga)
  }

  handleEditSollstaerkeOnMannschaft = (id, sollstaerke) => {
    this.model.planung.editMannschaftSollstaerke(id, sollstaerke)
  }

  handleEditSpieltagOnMannschaft = (id, spieltag) => {
    this.model.planung.editMannschaftSpieltag(id, spieltag)
  }

  handleEditUhrzeitOnMannschaft = (id, uhrzeit) => {
    this.model.planung.editMannschaftUhrzeit(id, uhrzeit)
  }

  handleEditSpielwocheOnMannschaft = (id, spielwoche) => {
    this.model.planung.editMannschaftSpielwoche(id, spielwoche)
  }

  handleEditKommentarOnMannschaft = (id, kommentar) => {
    this.model.planung.editMannschaftKommentar(id, kommentar)
  }

  handleClickDeleteButtonOnMannschaft = (id, keep_spieler) => {
    this.model.deleteMannschaft(id, keep_spieler)
  }

}