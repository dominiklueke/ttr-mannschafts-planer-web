export default class WelcomeModalView {

    constructor() {
      // fill html
      $('#welcomeModal').append(this._getHtml())
      // get jq element
      this.modal = $("#tischtennis-planer-welcome-modal")
      // react on buttons
      this.nextButton = $('#welcome-modal-next')
      this.nextButton.click((event) => {
        this._handleNextButtonClick(event)
      })
      this.backButton = $('#welcome-modal-back')
      this.backButton.click((event) => {
        this._handleBackButtonClick(event)
      })
      // react on modal close
      this.modal.on('hidden.bs.modal', (e) => {
        $('.welcome-modal-page.active').removeClass('active')
        $('.welcome-modal-page[data-page="1"]').addClass('active')
        localStorage.setItem('skipWelcomeScreen', 'true')
      })
    }
  
    displayWelcomeModal() {
      if(!localStorage.getItem('skipWelcomeScreen')){
        $('.modal.show').modal('hide')
        this.modal.modal('show')
      }
    }
  
    /* PRIVATE */
    _getHtml() {
      return `
      <div class="modal fade" id="tischtennis-planer-welcome-modal" tabindex="-1" role="dialog" aria-labelledby="tischtennis-planer-welcome-modal-label" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" style="font-weight:300;" id="tischtennis-planer-welcome-modal-label">Herzlich Willkommen</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body p-4">
              <div class="welcome-modal-page active" data-page="1">
                <p>
                  Vielen Dank, dass Du den TTR-Mannschafts-Planer verwendest!
                </p>
                <p>
                  <img src="./images/appicon.PNG" width="33%">
                </p>
                <p>
                  Bevor es losgeht, bitte beachte noch folgende technische Hinweise.
                </p>
              </div>
              <div class="welcome-modal-page" data-page="2">
                <h1 class="welcome-icon"><i class="fa fa-shield"></i></h1>
                <p>
                  Diese Webapp sammelt keinerlei Daten und speichert deine Planung auch auf keinem Server.
                </p>
              </div>
              <div class="welcome-modal-page" data-page="3">
                <h1 class="welcome-icon"><i class="fa fa-clock-o"></i></h1>
                <p>
                  Alle notwendigen Daten deiner Planung werden <br/>
                  in deinem lokalen Browser-Cache gespeichert.
                </p>
                <p class="alert alert-secondary">
                  <i class="fa fa-refresh"></i>
                  Du kannst diese Seite während der Benutzung beliebig neu laden.
                  Deine Planung geht nicht verloren.
                </p>
              </div>
              <div class="welcome-modal-page" data-page="4">
                <h1 class="welcome-icon"><i class="fa fa-trash-o"></i></h1>
                <p>
                  Wird allerdings dein Browser-Cache geleert, <br/>
                  so ist auch deine Planung innerhalb der Webapp gelöscht.
                </p>
                <p class="alert alert-secondary">
                  <i class="fa fa-user-secret"></i>
                  Das kann z.B. passieren, wenn du im privaten Modus surfst und das Browserfenster schließt.
                </p>
              </div>
              <div class="welcome-modal-page" data-page="5">
                <h1 class="welcome-icon"><i class="fa fa-cloud-download""></i></h1>
                <p>
                  Achte also darauf, deine Planung regelmäßig über das Menü als <code>.ttsp</code>-Datei auf deinem Rechner zu sichern.
                </p>
                </div>
              <div class="welcome-modal-page" data-page="6">
                <p>
                  <img src="./images/appicon.PNG" width="33%">
                </p>
                <p>
                  Und nun viel Spaß beim Planen!
                </p>
              </div>
              <button id="welcome-modal-next" class="btn btn-success pull-right mt-3">Weiter</button>
              <button id="welcome-modal-back" class="btn btn-outline-dark pull-right mt-3 mr-2">Nein danke</button>
            </div>
          </div>
        </div>
      </div>
    `}

    _handleNextButtonClick(){
      var currentPage = parseInt($('.welcome-modal-page.active').attr('data-page'), 10)
      var lastPage = $('.welcome-modal-page').size()

      if(currentPage == lastPage){
        this.modal.modal('hide')
      } else {
        this._updatePage(currentPage + 1, lastPage)
      }
    }

    _handleBackButtonClick(){
      var currentPage = parseInt($('.welcome-modal-page.active').attr('data-page'), 10)
      var lastPage = $('.welcome-modal-page').size()
      if(currentPage == 1){
        this.modal.modal('hide')
      } else {
        this._updatePage(currentPage - 1, lastPage)
      }
    }

    _updatePage(newPage, lastPage) {
      $('.welcome-modal-page.active').removeClass('active')
      $('.welcome-modal-page[data-page="' + newPage + '"]').addClass('active')
      var prevPage = newPage - 1;
      var nextPage = newPage + 1;
      if(prevPage < 1){
        this.backButton.text("Nein danke")
      } else {
        this.backButton.text("Zurück")
      }
      if(nextPage > lastPage){
        this.nextButton.text("Los geht's")
      } else {
        this.nextButton.text("Weiter")
      }
    }

  }