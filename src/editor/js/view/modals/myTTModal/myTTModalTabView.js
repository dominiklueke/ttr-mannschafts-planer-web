export default class MyTTModalTabView {
  constructor(container, id) {
    container.find("ul.nav").append(`
      <li class="nav-item pl-1">
        <a class="nav-link" id="myttmodal-${id}-tab" href="#myttmodal-${id}-pane" data-toggle="tab" role="tab" aria-controls="myttmodal-${id}-tab" aria-selected="true">${id}</a>
      </li>
    `)
    container.find("div.tab-content").append(`
      <div class="tab-pane fade" id="myttmodal-${id}-pane" role="tabpanel" aria-labelledby="${id}-pane">
        <div id="myttmodal-${id}-container" class="container border border-success rounded mt-4 pt-4 mb-2 bg-light text-muted">
          <div class="row">
            <div class="col" id="myttmodal-${id}-anleitung">
              <h4>Anleitung</h4>
              <p><strong><span class="text-success">1.</span></strong> Öffne die <a id="myttmodal-${id}-home-url-link" class="text-success" >${id}</a> bei myTischtennis in einem neuen Browser-Tab.</p>
              <p><strong><span class="text-success">2.</span></strong> Speichere die geöffnete Webseite mit deinem Browser als <strong>.html</strong> Datei auf deinem Rechner. (Tastenkürzel <code>Strg+S</code>)</p>
              <div class="alert alert-warning">
                <i class="fa fa-warning text-warning"></i> Beim Speichern den Dateityp <code>Webseite, komplett</code> auswählen)
                <br/>
                <img src="./images/myTT-Speichern-Unter.png" width="100%">
              </div>
              <p><strong><span class="text-success">3.</span></strong> Lade die heruntergeladene <code>.html</code> Datei hier hoch:</p>
              <form id="myttmodal-${id}-dropzone" action="/" class="dropzone">
                <div class="fallback">
                  <input name="file" type="file" />
                </div>
              </form>
              <p></p>
              <p><strong><span class="text-success">4.</span></strong> Die hochgeladenen Daten werden angezeigt, analysiert und können geladen werden</p>
            </div>
            <div class="col d-none" id="myttmodal-${id}-preview">
            </div>
          </div>
        </div>
        <div id="myttmodal-${id}-parse-results-container" class="container d-none">
          <div class="row text-muted mb-3">
            <p id="myttmodal-${id}-status" class="myttmodal-status ml-2">
            </p>
          </div>
          <div class="row">
            <div class="col-sm p-0">
              <span class="d-inline" id="myttmodal-${id}-upload-new-file-button-popover-container" >
                <button id="myttmodal-${id}-upload-new-file-button" type="button" class="btn btn-outline-success">Neue Datei <span class="d-none d-md-inline">hochladen</span></button>
              </span>
              <span class="d-inline pull-right" id="myttmodal-${id}-load-button-popover-container" >
                <button id="myttmodal-${id}-load-button" type="button" class="btn btn-success pull-right" disabled >${id} laden</button>
              </span>
            </div>
          </div>
        </div>
      </div>
    `)
    Dropzone.autoDiscover = false
    this.dropzone = new Dropzone(`#myttmodal-${id}-dropzone`)
    this.dropzone.on("complete", (file) => {
      const reader = new FileReader();
      reader.addEventListener('load', (event) => {
        try {
          this.status_row.html(`<small>Suche ${this.id}</small> <div class="spinner-grow spinner-grow-sm" role="status"><span class="sr-only">Lade Informationen...</span></div>`)
          setTimeout(() => {
            // preparse html from file
            var preParseResult = this.preParseHtml(event.target.result)
            // display preparse results as preview
            this.anleitung_container.addClass("d-none")
            this.preview_container.html(`<h6>${preParseResult.displayHeader}</h6>${preParseResult.displayBody}`)
            this.preview_container.removeClass("d-none")
            // do the full parsing and display results
            this.planung = this.parseHtml(preParseResult.url, preParseResult.htmlBodyForParsing)
            this._displayParseResult()
          }, 500)
        } catch (e) {
          console.log(`An error ocurred reading the file ${file.name}:<br/>${e}`)
        }
      });
      reader.readAsText(file)
    });

    this.id = id
    // cache jq elements
    this.modal = $("#planung-reload-data-modal")
    this.tab = $(`#myttmodal-${id}-tab`)
    this.container = $(`#myttmodal-${id}-container`)
    this.anleitung_container =  $(`#myttmodal-${id}-anleitung`)
    this.preview_container = $(`#myttmodal-${id}-preview`)
    this.parse_results_container = $(`#myttmodal-${id}-parse-results-container`)
    this.home_url_link = $(`#myttmodal-${id}-home-url-link`)
    this.load_button = $(`#myttmodal-${id}-load-button`)
    this.load_button_popover = $(`#myttmodal-${id}-load-button-popover-container`)
    this.new_file_upload_button = $(`#myttmodal-${id}-upload-new-file-button`)
    this.new_file_upload_button_popover = $(`#myttmodal-${id}-upload-new-file-button-popover-container`)
    this.status_row = $(`#myttmodal-${id}-status`)
    // properties
    this.planung = {}
    // functions
    this.preParseHtml = {}
    this.parseHtml = {}
    this.getParseResult = {}
    //tab activation
    this.tab.on('shown.bs.tab', (event) => {
    })
    this.new_file_upload_button.click((event) => {
      this._displayUploadForm()
    })
    // inital disply of upload form
    this._displayUploadForm()
  }

  bindHtmlPreParser(parser) {
    this.preParseHtml = parser
  }

  bindHtmlParser(parser) {
    this.parseHtml = parser
  }

  bindParseResultAnalyzer(analyzer) {
    this.getParseResult = analyzer
  }

  bindClickOnLoadButtonOnMyTTModalTab(handler) {
    this.load_button.click((event) => { 
      this.load_button_popover.popover('dispose')
      this._loadData(handler) 
    })
  }

  show() {
    this.tab.tab('show')
  }

  setHomeUrl(url) {
    this.home_url_link.attr("href", url)
  }


  notifyPlanungUpdated() {
    if (Object.entries(this.planung).length > 0) {
      this._displayParseResult()
    }
  }

  _loadData(handler) {
    this.load_button.html('<i class="fa fa-circle-o-notch fa-spin"></i>')
    setTimeout( () => {
      handler(this.planung)
      this.load_button.html(`Lade ${this.id}`)
      this.modal.modal('hide')
    }, 1000)
  }
  _displayParseResult() {
    const parse_result = this.getParseResult(this.planung)
    this.status_row.html(`<small>${parse_result.html}</small>`)
    // enable/disable button
    if ( parse_result.result ) {
      this.load_button.removeProp("disabled")
      this.load_button.attr("style", "")
    } else {
      this.load_button.prop("disabled", true)
      this.load_button.attr("style", "pointer-events: none;")
    }
    // Add popover to button if we are about to change the planungs details with 'Aufstellung laden'
    this.load_button_popover.popover('dispose')
    if ( parse_result.popoverhtml ) {
      this.load_button_popover.popover({
        trigger: 'hover',
        content: parse_result.popoverhtml,
        html: true,
        placement: 'top',
        template: '<div class="popover myttmodal-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
      })
    }
    this.container.css("height","calc(85vh - 16em)").css("overflow-y", "scroll")
    this.parse_results_container.removeClass("d-none")
  }

  _displayUploadForm() {
    this.dropzone.removeAllFiles(true)
    this.anleitung_container.removeClass("d-none")
    this.preview_container.html(``)
    this.preview_container.addClass("d-none")
    this.parse_results_container.addClass("d-none")
    this.container.css("height","calc(85vh - 6em)").css("overflow-y", "auto")
  }
}