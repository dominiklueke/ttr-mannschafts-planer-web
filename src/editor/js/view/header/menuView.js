export default class MenuView {

  constructor() {
    $('#navbarCollapseLeft').append(`
        <ul class="navbar-nav">
          <li class="nav-item nav-link text-white">
            <a id="new-planung-button">
              <i class="fa fa-file-o"></i>
              <span class="pl-2 d-inline">Neue Planung</span>
            </a>
          </li>
          <li class="nav-item nav-link text-white">
            <a id="open-planung-button">
              <i class="fa fa-folder-open-o"></i>
              <span class="pl-2 d-inline">Öffne Planung</span>
            </a>
            <input type="file" id="file-upload-menu" accept=".ttsp" class="d-none">
          </li>
          <li class="nav-item nav-link text-white">
            <a id="save-planung-button">
              <i class="fa fa-cloud-download"></i>
              <span class="pl-2 d-inline">Speichern</span>
            </a>
          </li>
          <li class="nav-item nav-link text-white">
            <a id="print-planung-button">
              <i class="fa fa-print"></i>
              <span class="pl-2 d-inline">Drucken</span>
            </a>
          </li>
          <li class="nav-item nav-link text-white">
            <a id="about-button">
              <i class="fa fa-info-circle"></i>
              <span class="pl-2 d-inline">Über</span>
            </a>
          </li>
          <li class="nav-item nav-link text-white">
            <a id="help-button" class="text-white" href="https://bitbucket.org/dominiklueke/ttr-mannschafts-planer-2.0/src/master/assets/help/HELP.md">
              <i class="fa fa-question-circle"></i>
              <span class="pl-2 d-inline">Hilfe</span>
            </a>
          </li>
          <li class="nav-item nav-link text-white">
            <a id="welcome-button">
              <i class="fa fa-lightbulb-o"></i>
              <span class="pl-2 d-inline">Willkommen</span>
            </a>
          </li>
          <li class="nav-item nav-link text-white">
            <a id="close-planung-button">
              <i class="fa fa-close"></i>
              <span class="pl-2 d-inline">Planung schließen</span>
            </a>
          </li>
        </ul>
      `
    )
    this.menu = $('#menu')
    this.newButton = $("#new-planung-button")
    this.openButton = $("#open-planung-button")
    this.fileUpload = $("#file-upload-menu")
    this.saveButton = $("#save-planung-button")
    this.printButton = $("#print-planung-button")
    this.aboutButton  = $("#about-button")
    this.helpButton  = $("#help-button")
    this.welcomeButton = $("#welcome-button")
    this.closeButton = $("#close-planung-button")
    this.createTooltips()
  }

  bindClickOnNewPlanungButton(handler) {
    this.newButton.click( (event) => {
      handler()
    })
  }

  bindClickOnSaveButton(handler) {
    this.saveButton.click( (event) => {
      handler()
    })
  }

  bindClickOnOpenButton(handler) {
    this.openButton.click( (event) => {
      event.preventDefault();
      this.fileUpload.trigger('click');
    })
    this.fileUpload.change((event) => {
      const files = event.target.files;
      handler(files)
    });
  }

  bindClickOnPrintButton(handler) {
    this.printButton.click( (event) => {
      handler()
    })
  }

  bindClickOnCloseButton(handler) {
    this.closeButton.click( (event) => {
      handler()
    })
  }

  bindClickOnAboutButton(handler) {
    this.aboutButton.click( (event) => {
      handler()
    })
  }

  bindClickOnWelcomeButton(handler) {
    this.welcomeButton.click( (event) => {
      handler(true)
    })
  }

  createTooltips() {
    this.newButton.attr("title", `Starte eine neue Planung`).tooltip('dispose').tooltip();
    this.openButton.attr("title", `Öffne eine Planung von deinem Rechner`).tooltip('dispose').tooltip();
    this.saveButton .attr("title", `Sichere die Planung auf deinem Rechner`).tooltip('dispose').tooltip();
    this.printButton.attr("title", `Drucke die Planung (als PDF) aus. Für Farben aktiviere je nach Browser die Hintergrundgrafiken`).tooltip('dispose').tooltip();
    this.aboutButton.attr("title", `Über`).tooltip('dispose').tooltip();
    this.helpButton.attr("title", `Hilfe`).tooltip('dispose').tooltip();
    this.welcomeButton.attr("title", `Zeige die Willkommens-Hinweise`).tooltip('dispose').tooltip();
    this.closeButton.attr("title", `Schließe die aktuelle Planung`).tooltip('dispose').tooltip();
  }

}