import Controller from './controller/controller'

/**
 * Open External URLs
 */

$(document).on('click', 'a[href^="http"]', function(event) {
  event.preventDefault();
  window.open(this.href, '_blank');
});

/**
 * Get correct app height for mobile browsers
 */

const appHeight = () => {
  const doc = document.documentElement
  doc.style.setProperty('--app-height', `${window.innerHeight}px`)
}
window.addEventListener('resize', appHeight)
appHeight()


/*
* --------------
*     M V C
* --------------
*/
const app = new Controller()
if (localStorage.getItem('localStorageFilepathQuit')) {
  openPlanungFromFile(localStorage.getItem('localStorageFilepathQuit'))
}