/**
 * MY TT PARSER Functions
 */

export function _getMyTTIdOfJqAWithDataBind(jq_a) {
    const data_bind_attr = JSON.parse( 
        ( typeof jq_a.attr("data-bind") !== typeof undefined && jq_a.attr("data-bind") !== false ) ? 
        jq_a.attr("data-bind")
        .replace("playerPopover: ","")
        .replace("personId","\"personId\"")
        .replace("clubNr", "\"clubNr\"")
        .replace(/'/g,"\"") : "{}"
    )
    return parseInt( ( "personId" in data_bind_attr ? data_bind_attr.personId : "").replace("NU",""), 10)
}

export function _getStatusIcon(status) {
    const icon = status == "success" ? "check" : status == "danger" ? "times" : status == "warning" ? "warning" : ""
    return `<i class="fa fa-${icon} text-${status}"></i>`
}

export function _sanitizeJQueryHtmlDocument(htmlDocument) {
    for (let scriptTag of htmlDocument.find('script')) {
        scriptTag.remove()
    }
    for (let linkTag of htmlDocument.find('link')) {
        $(linkTag).attr("target", "_blank")
    }
    for (let imgTag of htmlDocument.find('img')) {
        imgTag.remove()
    }
    for (let aTag of htmlDocument.find('a')) {
        $(aTag).attr("target", "_blank")
    }
}

export function _getCurrentUrl(htmlDocument) {
    var url = ""
    var link = $(htmlDocument.find('#theMainColumnFromLayout .btn-group a.active')).attr('href')
    var mytt_baseurl = "https://www.mytischtennis.de"
    if (!link.startsWith(mytt_baseurl)) {
      url = mytt_baseurl
    }
    url += link
    return url
}