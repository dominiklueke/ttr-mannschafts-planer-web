import { _getMyTTIdOfJqAWithDataBind, _getStatusIcon, _sanitizeJQueryHtmlDocument, _getCurrentUrl } from './../parser/myTTParserFunctions'
import { SPIELKLASSEN } from './../constants'
export default class MyTTBilanzenParser {

  /**
   * BILANZEN
   */

  /**
   * return a json object that can be loaded as or into a PlanungsModel or {} if parsing fails
   * @param {*} url MyTischtennisUrl
   * @param {*} html The html belonging to the url
   */
  parse(url, html) {
    // init return value
    var planung = {
      mannschaften: {
        liste: []
      },
      spieler: {
        liste: []
      },
      bilanzen: {
        status: "ok",
        saisons: []
      }
    }
    try {
      /* get information from url */
      planung = this.parseUrl(url, planung)
      /* get information from html */
      planung = this.parseHtml(html, planung)
    } catch (e) {
      // Return empty if parsing error
      planung = {}
    }
    /* return */
    return planung
  }

  /**
   * Extract the saison, halbserie and verband from a vaild myTischtennis Bilanzen URL
   * @param {*} url 
   * @param {*} planung 
   */
  parseUrl(url, planung){
    
    const url_split = url.split("/") 
    // Expect like "https://www.mytischtennis.de/clicktt/WTTV/19-20/verein/187012/TuRa-Elsen/bilanzen/rr"
    // url_split = [https:,,www.mytischtennis.de,clicktt,WTTV,19-20,verein,187012,TuRa-Elsen,bilanzen,rr]
    if (url_split.length <= 13){
      // verband
      if (url_split.length > 4) {
        planung.verband = url_split[4]
      }
      // saison
      if (url_split.length > 5 && (url_split[5]).match(/\d\d-\d\d/g) !== null ) {
        planung.bilanzsaison = "20" + url_split[5].replace("-","/")
      }
      // serie
      if (url_split.length > 10) {
        const halbserie = url_split[10].replace("rr", "Rückrunde").replace("vr","Vorrunde").replace("gesamt", "Gesamt")
        if (halbserie == "Vorrunde" || halbserie == "Rückrunde" || halbserie == "Gesamt") {
          planung.bilanzhalbserie = halbserie
        }
      }
    }
    planung.bilanzen.saisons.push(`${planung.bilanzhalbserie} ${planung.bilanzsaison}`)
    return planung
  }

  /**
   * preParse the uploaded html to get necessary information before parsing completely
   * @param {*} html 
   */
  preParseHtml(html) {
    var url = ""
    var displayHeader = ""
    var displayBody = ""
    var htmlBodyAsString = ""
    try {
      var jq = $('<div></div>');
      jq.html(html);
      _sanitizeJQueryHtmlDocument(jq)
      htmlBodyAsString = $(jq.find('#theMainColumnFromLayout')).prop('innerHTML')
      var mannschaftsheader = jq.find('#theMainColumnFromLayout .panel-body > h3:not(.panel-title)')
      var gamestatstables = jq.find('#theMainColumnFromLayout .panel-body > table.table-mytt')
      for (var i=0; i<mannschaftsheader.length; ++i) {
        displayBody += `<h6 class="text-muted">${$(mannschaftsheader[i]).prop('textContent')}</h6>${$(gamestatstables[i]).prop('outerHTML')}`
      }
      url = _getCurrentUrl(jq)
      var saison = "20" + url.split("/")[5].replace("-","/");
      var halbserie = $(jq.find('#theMainColumnFromLayout .btn-group a.active')).prop('textContent');
      displayHeader = `${halbserie} ${saison}`
    } catch (e) {
      console.log(e.message)
    }
    return {
      url: url,
      displayHeader: displayHeader, 
      displayBody: displayBody, 
      htmlBodyForParsing: htmlBodyAsString
    }
  }

  /**
   * Return the given planungs json filled with the found information in the mytischtennis bilanzen html (mannschaften, spieler, etc)
   * @param {*} url 
   * @param {*} planung 
   */
  parseHtml(html, planung) {
    var jq = $('<div></div>');
    jq.html(`<html><head></head><body>${html}</body></html>`);
    // verein
    const verein_verband = jq.find(".panel-body > h1").first().html().split(" <small>") // "TuRa Elsen <small>WTTV</small>"
    const vereinsNummer = jq.find(".panel-body > h5").first().text().split(", ")[0].split(": ")[1] // "VNr.: 187012, Gründungsjahr: 1947"
    if (verein_verband.length == 2 && vereinsNummer && vereinsNummer.match(/\d*/g) !== null){
      planung.verein = verein_verband[0]
      planung.vereinsNummer = vereinsNummer
    }
    const saison_id = `${planung.bilanzhalbserie}-${planung.bilanzsaison}`
    //loop over all mannschaften
    const mannschaften_tables = jq.find("table#gamestatsTable")
    for (var i = 0; i < mannschaften_tables.length; i++) {
      var mannschaften_table = $(mannschaften_tables[i])
      // get the spielklasse of the current bilanzen table from the preceding h3
      var mannschaften_table_header = mannschaften_table.prev("h3").find("a").html() // TuRa Elsen Herren II [(4er)] <i/> (Rückrunde)
      var mannschaft_link = mannschaften_table.prev("h3").find("a").attr("href")
      var mannschaft_name = mannschaften_table_header.split("<i")[0].trim() + " " // TuRa Elsen Herren II [(4er)] add trailing whitespace, we need it to safely find the roman number
      var roman_number = 'I'
      const valid_roman_numbers = ['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV']
      for(var j=0; j<valid_roman_numbers.length; j++){
        if(mannschaft_name.includes(` ${valid_roman_numbers[j]} `)) {
          roman_number = valid_roman_numbers[j]
        }
      }
      // detect spielklasse by searching through all valid spielklassen
      // hint: this is necessary because the verein name in page heading and the name above each table can be different
      // e.g. "Sportverein 1930 Bergheim" vs. "SV Bergheim"
      var tmp_spielklasse = mannschaft_name.replace(roman_number, '').split("(")[0].trim() // TuRa Elsen Herren
      var spielklasse = ''
      for(var [id, sk_group] of Object.entries(SPIELKLASSEN)) {
        for(var [id2, sk] of Object.entries(sk_group)) {
          if(tmp_spielklasse.endsWith(` ${sk.name}`)) {
            spielklasse = sk.name
          }
        }
      }
      if(spielklasse === '') {
        continue
      }
      mannschaft_name = `${spielklasse}${roman_number === 'I' ? '' : (' ' + roman_number)}`
      // initialize the mannschaft
      var mannschaft = {
        spielklasse: spielklasse,
        romanNumber: roman_number,
        bilanzen: { }
      }
      // sometimes the same mannschaft is duplicate in the html from myTT.
      // only add it once to the bilanzen
      if (planung.mannschaften.liste.filter(m => m.spielklasse == mannschaft.spielklasse && m.romanNumber == mannschaft.romanNumber).length > 0) {
        continue;
      }
      // loop over all rows in the table
      var bilanzen_trs = mannschaften_table.children("tbody").children("tr:not(.collapse)") // only get first level trs
      for (var j = 0; j < bilanzen_trs.length; j++) {
        var bilanzen_tr = $(bilanzen_trs[j])
        // initialize the spieler
        var spieler = {
          spielklasse: spielklasse,
          bilanzen: { }
        }
        var spieler_mannschafts_bilanz = {}
        // loop over all tds in the row
        var spieler_tds = bilanzen_tr.children("td") // get the tds for the spieler
        for (var k = 0; k < spieler_tds.length; k++) {
          var spieler_td = $(spieler_tds[k])
          var spieler_is_valid = true
          switch (k) {
            case 0: //rang
              var rang_text = spieler_td.text().trim()
              if (!rang_text || /^\s*$/.test(rang_text)) {  // skip if the rang is empty and we have no spieler
                spieler_is_valid = false
                break
              }
              var rang = rang_text.split(".")
              var einsatz_mannschaft = parseInt( rang[0], 10)
              var einsatz_position = parseInt( rang[1], 10)
              // init bilanz for this spieler for this saison
              spieler_mannschafts_bilanz = {
                einsatz_mannschaft: mannschaft_name,
                rang: rang_text
              }
              break;
            case 1: // name + mytt_id
              // <a role="button" tabindex="0" href="#" data-bind="playerPopover: { personId: 'NU1234567', clubNr: '123456' }" data-original-title="" title="">Nachname, Vorname</a>
              const a = spieler_td.find("a")
              spieler.name = a.text().trim()
              spieler.mytt_id = _getMyTTIdOfJqAWithDataBind(a)
              spieler_mannschafts_bilanz.name = spieler.name
              break;
            case 2: //Einsätze
              const einsaetze = parseInt( spieler_td.text().trim() )
              spieler_mannschafts_bilanz.einsaetze = einsaetze
              break
            case 3: // Bilanz gg Position 1
              const bilanz1 = spieler_td.text().trim()
              spieler_mannschafts_bilanz[1] = bilanz1
              break
            case 4: // Bilanz gg Position 2
              const bilanz2 = spieler_td.text().trim()
              spieler_mannschafts_bilanz[2] = bilanz2
              break
            case 5: // Bilanz gg Position 3
              const bilanz3 = spieler_td.text().trim()
              spieler_mannschafts_bilanz[3] = bilanz3
              break
            case 6: // Bilanz gg Position 4
              const bilanz4 = spieler_td.text().trim()
              spieler_mannschafts_bilanz[4] = bilanz4
              break
            case 7: // Bilanz gg Position 5
              const bilanz5 = spieler_td.text().trim()
              spieler_mannschafts_bilanz[5] = bilanz5
              break
            case 8: // Bilanz gg Position 6
              const bilanz6 = spieler_td.text().trim()
              spieler_mannschafts_bilanz[6] = bilanz6
              break
            case 9: // Bilanz gesamt
              const bilanzgesamt = spieler_td.text().trim().replace(/\s+/g, '');
              spieler_mannschafts_bilanz.gesamt = bilanzgesamt
              break
            default:
              break
          }
          if (!spieler_is_valid) { break }
        }
        if (spieler_is_valid){
          // Now we can check if we already had this spieler
          // if yes, we start to fill the already found spieler with the current bilanzen
          var found_spieler_arr = planung.spieler.liste.filter(findspieler => (findspieler.mytt_id == spieler.mytt_id))
          if ( found_spieler_arr.length > 0 ) {
            found_spieler_arr.forEach(foundspieler => {
              foundspieler.bilanzen[saison_id].bilanzen.push(spieler_mannschafts_bilanz)
            })
          } else {
            // Init the spieler bilanzen
            spieler.bilanzen[saison_id] = {
              saison: planung.bilanzsaison,
              halbserie: planung.bilanzhalbserie,
              position: `${einsatz_mannschaft}.${einsatz_position}`,
              bilanzen: []
            }
            spieler.bilanzen[saison_id].bilanzen.push(spieler_mannschafts_bilanz)
            planung.spieler.liste.push(spieler)
          }
          // put the spieler also in the mannschafts bilanz
          var found_mannschaft = planung.mannschaften.liste
          .filter(findmannschaft => (findmannschaft.spielklasse == mannschaft.spielklasse))
          .find(findmannschaft => (findmannschaft.romanNumber == mannschaft.romanNumber))
          if (found_mannschaft){
            found_mannschaft.bilanzen[saison_id].bilanzen.push(spieler_mannschafts_bilanz)
          } else {
            mannschaft.bilanzen[saison_id] = {
              saison: planung.bilanzsaison,
              halbserie: planung.bilanzhalbserie,
              url: mannschaft_link.includes("mytischtennis.de") ? mannschaft_link : `https://mytischtennis.de${mannschaft_link}`,
              bilanzen: []
            }
            mannschaft.bilanzen[saison_id].bilanzen.push(spieler_mannschafts_bilanz)
            planung.mannschaften.liste.push(mannschaft)
          }
        }
      }
    }
    return planung
  }

  /**
   * Analyze the planungs object and return 
   * {result: [true|false], html: <html-string to display}
   * the result is true if the planungs object is a loadable aufstellung
   * @param {*} planung 
   */
  getResult(planung, current_planung){
    var bilanzenFound = true
    var statusHtml = ""
    // verein
    if (
      ("verein" in planung && planung.verein == current_planung.verein) || 
      ("vereinsNummer" in planung && planung.vereinsNummer == current_planung.vereinsNummer)) {
      statusHtml += `Verein: ${planung.verein} ${_getStatusIcon("success") } `
    } else {
      statusHtml += `Verein: ${planung.verein} (Nicht ${current_planung.verein}) ${_getStatusIcon("danger") } `
      bilanzenFound = false
    }
    statusHtml += "<br/>"
    // saison
    if ("bilanzsaison" in planung && "bilanzhalbserie" in planung) {
      statusHtml += `Saison: ${planung.bilanzhalbserie} ${planung.bilanzsaison} ${_getStatusIcon("success") } `
    } else {
      statusHtml += `Keine Saison ${_getStatusIcon("danger") } `
      bilanzenFound = false
    }
    statusHtml += "<br/>"
    // mannschaften
    if ("mannschaften" in planung && planung.mannschaften.liste.length > 0) {
      statusHtml += `${planung.mannschaften.liste.length} Mannschaften ${_getStatusIcon("success") } `
    } else {
      statusHtml += `Keine Mannschaften ${_getStatusIcon("danger") } `
      bilanzenFound = false
    }
    // spieler
    if ("spieler" in planung && planung.spieler.liste.length > 0) {
      statusHtml += `${planung.spieler.liste.length} Spieler ${_getStatusIcon("success") } `
    } else {
      statusHtml += `Keine Spieler ${_getStatusIcon("danger") } `
      bilanzenFound = false
    }
    // information for popover
    var popoverhtml = ''
    var update_mannschaften_arr = []
    var update_spieler_arr = []
    if (bilanzenFound) {
      update_mannschaften_arr = current_planung.mannschaften.liste.filter( mannschaft => ( planung.mannschaften.liste.find( mannschaft1 => mannschaft1.spielklasse === mannschaft.spielklasse && mannschaft1.romanNumber === mannschaft.romanNumber) !== undefined ) )
      update_spieler_arr = current_planung.spieler.liste.filter( spieler => ( planung.spieler.liste.find( spieler1 => spieler1.mytt_id === spieler.mytt_id) !== undefined ) )
    }
    if (update_spieler_arr.length > 0 && update_mannschaften_arr.length > 0){
      popoverhtml = '<h6>Die Planung wird aktualisiert.</h6>'
      popoverhtml += `<i class="fa fa-refresh text-primary"></i> Für <b>${update_mannschaften_arr.length} Mannschaften</b> der aktuellen Planung werden Bilanzen aktualisiert.<br/>`
      popoverhtml += `<i class="fa fa-refresh text-primary"></i> Für <b>${update_spieler_arr.length} Spieler</b> der aktuellen Planung werden Bilanzen aktualisiert.`
    } else {
      popoverhtml = '<i class="fa fa-warning text-warning"></i> Es werden für <b/>keine</b> Spieler Bilanzen aktualisiert.<br/>'
      popoverhtml += `<br/><b>Bitte lade zunächst eine Aufstellung von myTischtennis.de.</b>`
      bilanzenFound = false
    }
    return { result: bilanzenFound, html: statusHtml, popoverhtml: popoverhtml }
  }

}