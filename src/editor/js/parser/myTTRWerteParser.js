import { _getMyTTIdOfJqAWithDataBind, _getStatusIcon, _sanitizeHtmlDocument, _sanitizeJQueryHtmlDocument } from './../parser/myTTParserFunctions'
export default class MyTTRWerteParser {

  /**
   * TTR-RANGLISTE
   */

   /**
   * return a json object that can be loaded as or into a PlanungsModel or {} if parsing fails
   * @param {*} url MyTischtennisUrl
   * @param {*} html The html belonging to the url
   */
  parse(url, html) {
    // init return value
    var planung = {
      spieler: {
        liste: []
      },
      ttrwerte: {
        url: url,
        status: "ok",
        date: null,
        aktuell: "Aktuell",
        datestring: ""
      }
    }
    /* get information from url */
    //planung = this.parseMyTTAufstellungsUrl(url, planung)
    /* get information from html */
    try {
      planung = this.parseHtml(html, planung)
    } catch (e) {
      // Return emtpy if parsing error
      planung = {}
    }
    /* return */
    return planung
  }

  /**
   * preParse the uploaded html to get necessary information before parsing completely
   * @param {*} html 
   */
  preParseHtml(html) {
    var url = ""
    var displayHeader = ""
    var displayBody = ""
    var htmlBodyAsString = ""
    try {
      var jq = $('<div></div>');
      jq.html(html);
      _sanitizeJQueryHtmlDocument(jq)
      htmlBodyAsString = $(jq.find('#rankingList')).prop('outerHTML')
      displayBody = $(jq.find('#rankingList table.table-mytt')).prop('outerHTML')
      var headerLabels = jq.find('#theMainColumnFromLayout .well span.label')
      for (let label of headerLabels)
      {
        displayHeader += `<span class="badge badge-ttr text-light p-1">${$(label).prop('textContent')}</span> `
      }
    } catch (e) {
      console.log(e.message)
    }
    return {
      url: url,
      displayHeader: displayHeader, 
      displayBody: displayBody, 
      htmlBodyForParsing: htmlBodyAsString
    }
  }

  /**
   * Return the given planungs json filled with the found information in the mytischtennis ttr-rangliste html
   * @param {*} html 
   * @param {*} planung 
  */
  parseHtml(html, planung) {
    var jq = $('<div></div>');
    jq.html(`<html><head></head><body>${html}</body></html>`);
    // check if rangliste is still loading
    if (jq.find("div#rankingList").length > 0 ) {
      if ( jq.find("div#rankingList table.table-mytt").length == 0 ) {
        planung.ttrrangliste_still_loading = true
        return planung
      }
    } else if ( jq.find("form[action='/community/login/'][name='login']").length > 0 ) {
      // Login required
      planung.login_required = true
      return planung
    } else {
      // Not even a ttr-rangliste page
      return {}
    }
    // ttr or qttr
    var today = new Date(Date.now())
    var ttr_date = today
    const this_year = ttr_date.getFullYear()
    const header_ths = jq.find("div#rankingList table.table-mytt thead th") // header_ths = [Rang,D-Rang,Spieler,Verein,(Q-)TTR,<empty>]
    if ( $(header_ths[4]).text() == "Q-TTR" ) {
      [-11,1,4,7,11].forEach(month => {
        var year = this_year
        year -= month < 0 ? 1 : 0
        month = Math.abs(month)
        var qttr_stichtag = new Date(year, month, 11)
        if ( today.getTime() > qttr_stichtag.getTime()) { ttr_date = qttr_stichtag }
      })
      planung.ttrwerte.aktuell = "Q-TTR"
    }
    planung.ttrwerte.date = ttr_date
    planung.ttrwerte.datestring = `${planung.ttrwerte.date.getDate()}.${planung.ttrwerte.date.getMonth()+1}.${planung.ttrwerte.date.getFullYear()}`
    // spieler
    var verein = null
    const trs = jq.find("div#rankingList table.table-mytt tbody tr")
    for (var j = 0; j < trs.length; j++) {
      var tr = $(trs[j])
      const spieler = {
        qttr: 0,
        name: "",
        mytt_id: 0
      }
      const tds = tr.find("td")
      for (var i = 0; i < tds.length; i++) {
        var td = $(tds[i])
        switch (i) {
          case 0: // rang
          case 1: // d-rang
            break;
          case 2: // name + verein
            // name: <a class="person123456 user-popover no-link" role="button" tabindex="0" data-tooltipdata="123456;0;Vorname Nachname;true"><span class="makered123456">Vorname <strong>Nachname</strong></span></a>
            const name_a = td.find("a").first()
            const nachname = name_a.find("strong").text().trim()
            var vorname = name_a.text().trim()
            vorname = vorname.slice(0, vorname.length - nachname.length - 1)
            spieler.name = `${nachname}, ${vorname}`
            const tooltipdata = name_a.attr("data-tooltipdata").split(";")
            spieler.mytt_id = parseInt(tooltipdata[0], 10)
            // verein: <a href="showclubinfo?isclickTTClub=1&amp;clubid=187012&amp;organisation=WTTV">TuRa Elsen</a>
            const verein_a = td.find("span a")
            const found_verein = verein_a.text().trim()
            // Only set verein here when all spieler have the same verein, else the planung will be invalid
            verein = (verein == null || found_verein == verein) ? found_verein : false
            break;
          case 3: // verein
            break
          case 4: // (q-)ttr
            spieler.qttr = parseInt(td.text().trim(), 10)
            break;
          default:
            break;
        }
      }
      if ( "name" in spieler && spieler.name !== "" &&
           "qttr" in spieler && ! isNaN(spieler.qttr) && 
           "mytt_id" in spieler && ! isNaN(spieler.mytt_id)
      ) {
        // set qttr_date of spieler
        spieler.qttrdate = planung.ttrwerte.date
        spieler.qttrinfo = `TTR-Stichtag: ${planung.ttrwerte.datestring}<br/>(${planung.ttrwerte.aktuell})`
        planung.spieler.liste.push(spieler)
      }
      // Only set verein here when all spieler have the same verein, else the planung will be invalid
      if (verein) { planung.verein = verein }
    }
    return planung
  }

  /**
   * Analyze the planungs object and return 
   * {result: [true|false], html: <html-string to display}
   * the result is true if the planungs object is a loadable aufstellung
   * @param {*} planung 
   */
  getResult(planung, current_planung){
    var ttrranglisteFound = true
    var statusHtml = ""
    if ( "login_required" in planung ){
      return { result: false, html: `Bitte einloggen ${_getStatusIcon("warning")}` }
    }
    if ("ttrrangliste_still_loading" in planung) {
      return { result: false, html: `Suche TTR-Werte <div class="spinner-grow spinner-grow-sm" role="status"></div>` }
    }
    if ( $.isEmptyObject(planung) ) {
      return { result: false, html: `Keine TTR-Rangliste gefunden ${_getStatusIcon("danger") }` }
    }
    // verein + + vereinsNummer + verband
    if ("verein" in planung && planung.verein == current_planung.verein ) {// && "vereinsNummer" in planung && "verband" in planung) {
      statusHtml += `Verein: ${planung.verein} ${_getStatusIcon("success") } ` // (${planung.vereinsNummer} - ${planung.verband})
    } else {
      statusHtml += `Verein: ${planung.verein} (Nicht ${current_planung.verein}) ${_getStatusIcon("danger") } `
      ttrranglisteFound = false
    }
    statusHtml += "<br/>"
    // aktuelle oder qttr werte
    if ("ttrwerte" in planung && "aktuell" in planung.ttrwerte && "datestring" in planung.ttrwerte) {
      statusHtml += `Stichtag: ${planung.ttrwerte.datestring} (${planung.ttrwerte.aktuell}) ${_getStatusIcon("success") } `
    } else {
      statusHtml += `Keine TTR-Werte gefunden ${_getStatusIcon("danger") } `
    }
    statusHtml += "<br/>"
    // spieler
    if ("spieler" in planung && planung.spieler.liste.length > 0) {
      statusHtml += `${planung.spieler.liste.length} Spieler ${_getStatusIcon("success") } `
    } else {
      statusHtml += `Keine Spieler gefunden ${_getStatusIcon("danger") } `
      ttrranglisteFound = false
    }
    // information for popover
    var popoverhtml = '<h6>Die Planung wird aktualisiert.</h6>'
    // TTR Werte
    const loaded_ttr_date = planung.ttrwerte.date.getTime()
    const current_ttr_date = current_planung.ttrwerte.date.getTime()
    if (current_ttr_date > 0) {
      if (loaded_ttr_date < current_ttr_date) {
        popoverhtml += `<i class="fa fa-warning text-warning"></i> TTR-Stichtag: ${current_planung.ttrwerte.datestring} &rarr; <b>${planung.ttrwerte.datestring}</b><br/> `
      } else if (loaded_ttr_date > current_ttr_date) {
        popoverhtml += `<i class="fa fa-refresh text-primary"></i> TTR-Stichtag: ${current_planung.ttrwerte.datestring} &rarr; <b>${planung.ttrwerte.datestring}</b><br/> `
      }
    } else {
      popoverhtml += `<i class="fa fa-check-circle text-success"></i> TTR-Stichtag: <b>${planung.ttrwerte.datestring}</b><br/> `
    }
    // How many spieler are updated
    const update_spieler_arr = current_planung.spieler.liste.filter( spieler => ( planung.spieler.liste.find( spieler1 => spieler1.mytt_id === spieler.mytt_id) !== undefined ) )
    if (update_spieler_arr.length > 0){
      popoverhtml += `<i class="fa fa-refresh text-primary"></i> <b>${update_spieler_arr.length} Spieler</b> (von ${current_planung.spieler.liste.length}) der aktuellen Planung erhalten neue TTR-Werte.`
    } else {
      popoverhtml = '<i class="fa fa-warning text-warning"></i> Es werden für <b/>keine</b> Spieler TTR-Werte aktualisiert.<br/>'
      popoverhtml += `<br/><b>Bitte lade zunächst eine Aufstellung von myTischtennis.de.</b>`
      ttrranglisteFound = false
    }
    return { result: ttrranglisteFound, html: statusHtml, popoverhtml: popoverhtml }
  }

}