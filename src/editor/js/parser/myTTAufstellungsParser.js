import { _getMyTTIdOfJqAWithDataBind, _getStatusIcon, _sanitizeJQueryHtmlDocument, _getCurrentUrl } from './../parser/myTTParserFunctions'
import { SOLLSTAERKEN } from './../constants'
export default class MyTTAufstellungsParser {

  /**
   * AUFSTELLUNG
   */

  /**
   * return a json object that can be loaded as or into a PlanungsModel or {} if parsing fails
   * @param {*} url MyTischtennisUrl
   * @param {*} html The html belonging to the url
   */
  parse(url, html) {
    // init return value
    var planung = {
      mannschaften: {
        liste: []
      },
      url: {
        verein: "",
        saison: "",
        halbserie: "",
        spielklasse: ""
      },
      spieler: {
        liste: []
      },
      aufstellung: {
        url: url,
        status: "ok"
      },
      ttrwerte: {
        status: "ok",
        date: null,
        aktuell: "Q-TTR",
        datestring: ""
      }
    }
    try {
      /* get information from url */
      planung = this.parseUrl(url, planung)
      /* get information from html */
      planung = this.parseHtml(html, planung)
    } catch (e) {
      // Return empty if parsing error
      console.log(e)
      planung = {}
    }
    /* return */
    return planung
  }

  /**
   * Extract the saison, halbserie and verband from a vaild myTischtennis Aufstellungs URL
   * @param {*} url 
   * @param {*} planung 
   */
  parseUrl(url, planung = {}){
    const url_split = url.split("/") 
    // Expect like "https://www.mytischtennis.de/clicktt/WTTV/19-20/verein/187012/TuRa-Elsen/mannschaftsmeldungendetails/H/vr/"
    // url_split = [https:,,www.mytischtennis.de,clicktt,WTTV,19-20,verein,187012,TuRa-Elsen,mannschaftsmeldungendetails,H,vr,]
    var qttr_month
    var qttr_year
    if (url_split.length <= 13){
      // verband
      if (url_split.length > 4) {
        planung.verband = url_split[4]
      }
      // saison
      if (url_split.length > 5) {
        if ( (url_split[5]).match(/\d\d-\d\d/g) !== null ) {
          planung.saison = "20" + url_split[5].replace("-","/")
          planung.url.saison = url_split[5]
          qttr_year = parseInt(planung.saison.slice(0,4),10)
        }
      }
      // vereinsnummer
      if (url_split.length > 7) {
        if ( (url_split[7]).match(/\d*/g) !== null ) {
          planung.vereinsNummer = url_split[7]
        }
      }
      // verein
      if (url_split.length > 8) {
        planung.url.verein = url_split[8]
      }
      // mannschaftsmeldungen übersicht or mannschaftsmeldungendetails
      if (url_split.length > 9) {
        planung.auswahl_required = (url_split[9] == "mannschaftsmeldungen")
      }
      // spielklasse
      if (url_split.length > 10) {
        planung.url.spielklasse = url_split[10]
      }
      // serie
      if (url_split.length > 11) {
        const halbserie = url_split[11].replace("rr", "Rückrunde").replace("vr","Vorrunde")
        if (halbserie == "Vorrunde" || halbserie == "Rückrunde") {
          planung.halbserie = halbserie
          planung.url.halbserie = url_split[11]
          qttr_month = halbserie == "Vorrunde" ? 4 : 11 // 4 -> Mai; 11 -> Dez
        }
        // qttr-date
        if ( ! planung.hasOwnProperty('ttrwerte') ) {
          planung.ttrwerte = {}
        }
        planung.ttrwerte.date = new Date(qttr_year, qttr_month, 11)
        planung.ttrwerte.datestring = `${planung.ttrwerte.date.getDate()}.${planung.ttrwerte.date.getMonth()+1}.${planung.ttrwerte.date.getFullYear()}`
      }

    }
    return planung
  }

  /**
   * preParse the uploaded html to get necessary information before parsing completely
   * @param {*} html 
   */
  preParseHtml(html) {
    
    var url = ""
    var displayHeader = ""
    var displayBody = ""
    var htmlBodyAsString = ""
    try {
      var jq = $('<div></div>');
      jq.html(html);
      _sanitizeJQueryHtmlDocument(jq)
      htmlBodyAsString = $(jq.find('#theMainColumnFromLayout')).prop('innerHTML')
      displayBody = $(jq.find('#theMainColumnFromLayout table.table-mytt')).prop('outerHTML')
      var header = $(jq.find('#theMainColumnFromLayout .panel-body h3')).prop('textContent')
      url = _getCurrentUrl(jq)
      var saison = "20" + url.split("/")[5].replace("-","/");
      var halbserie = $(jq.find('#theMainColumnFromLayout .btn-group a.active')).prop('textContent');
      displayHeader = `${header}, ${halbserie} ${saison}`
    } catch (e) {
      console.log(e.message)
    }
    return {
      url: url,
      displayHeader: displayHeader, 
      displayBody: displayBody, 
      htmlBodyForParsing: htmlBodyAsString
    }
  }

  /**
   * Return the given planungs json filled with the found information in the mytischtennis aufstellungs html (mannschaften, spieler, etc)
   * @param {*} url 
   * @param {*} planung 
   */
  parseHtml(html, planung) {
    var jq = $('<div></div>');
    jq.html(`<html><head></head><body>${html}</body></html>`);
    // verein, spielklasse, verband, vereinsNummer
    const verein_verband = jq.find(".panel-body > h1").first().html().split(" <small>") // "TuRa Elsen<small>WTTV</small>"
    var verein_spielklasse = []
    var planung_spielklasse = ""
    var spieler_spielklasse = ""
    try{
      verein_spielklasse = jq.find(".panel-body > h3").first().text().split(", ") // "TuRa Elsen, Herren"
      planung_spielklasse = verein_spielklasse[1].split(" ")[0] // Herren or Jungen
      spieler_spielklasse = verein_spielklasse[1] // Herren or Jungen 18
    } catch {}
    const vereinsNummer = jq.find(".panel-body > h5").first().text().split(", ")[0].split(": ")[1] // "VNr.: 187012, Gründungsjahr: 1947"
    if (verein_spielklasse.length == 2 && vereinsNummer.match(/\d*/g) !== null){
      planung.verein = verein_verband[0].trim()
      planung.vereinsNummer = planung.vereinsNummer ? planung.vereinsNummer : vereinsNummer
      planung.spielklasse = planung.spielklasse ? planung.spielklasse : planung_spielklasse
      planung.spieler_spielklasse = spieler_spielklasse
    }
    // mannschaften, spieler
    const trs = jq.find("tbody tr")
    for (var j = 0; j < trs.length; j++) {
      var tr = $(trs[j])
      const spieler = {
        sbe: false,
        res: false,
        spv: {
          primary: false,
          secondary: 0
        }
      }
      const tds = tr.find("td")
      for (var i = 0; i < tds.length; i++) {
        var td = $(tds[i])
        switch (i) {
          case 0: //position
            var pos_text = td.text().trim().split(".")
            spieler.mannschaft = parseInt( pos_text[0], 10)
            spieler.position = parseInt( pos_text[1], 10)
            break;
          case 1: //qttr
            spieler.qttr = parseInt( td.text().trim(), 10)
            if (isNaN(spieler.qttr)) {spieler.qttr = 0}
            break;
          case 2: //name + mytt_id
            // <a role="button" tabindex="0" href="#" data-bind="playerPopover: { personId: 'NU1234567', clubNr: '123456' }" data-original-title="" title="">Nachname, Vorname</a>
            const a = td.find("a")
            spieler.name = a.text().trim()
            spieler.mytt_id = _getMyTTIdOfJqAWithDataBind(a)
            break;
          case 3: //hidden
            break;
          case 4: //status
            var status_text = td.text().split(",")
            status_text.forEach(status => {
              spieler.sbe = spieler.sbe || status.trim() == "SBE"
              spieler.reserve = spieler.reserve || status.trim() == "RES"
              spieler.spv.primary = spieler.spv.primary || status.trim() == "SPV"
            })
            break;
          default:
            break;
        }
      }
      if ( "name" in spieler && spieler.name !== "" &&
           "mannschaft" in spieler && ! isNaN(spieler.mannschaft) && 
           "position" in spieler &&  ! isNaN(spieler.position)
      ) {
        // set qttr_date of spieler
        spieler.qttrdate = planung.ttrwerte.date
        spieler.qttrinfo = `TTR-Stichtag: ${planung.ttrwerte.datestring}<br/>(${planung.ttrwerte.aktuell})`
        // set spielklasse of spieler
        spieler.spielklasse = spieler_spielklasse
        planung.spieler.liste.push(spieler)
        if ( spieler.mannschaft > planung.mannschaften.liste.length ) {
          planung.mannschaften.liste.push( {
            nummer: spieler.mannschaft,
            spielklasse: spieler_spielklasse,
            sollstaerke: SOLLSTAERKEN[planung_spielklasse]
          } )
        }
      }
    }
    return planung
  }

  /**
   * Analyze the planungs object and return 
   * {result: [true|false], html: <html-string to display}
   * the result is true if the planungs object is a loadable aufstellung
   * @param {*} planung 
   */
  getResult(planung, current_planung){
    var aufstellungFound = true
    var statusHtml = ""
    // verein + + vereinsNummer + verband
    if ("verein" in planung && "vereinsNummer" in planung && "verband" in planung) {
      if ( planung.verein == current_planung.verein ) {
        statusHtml += `Verein: ${planung.verein} (${planung.vereinsNummer} - ${planung.verband}) ${_getStatusIcon("success") } `
      } else {
        statusHtml += `Verein: ${planung.verein} (Nicht ${current_planung.verein}) ${_getStatusIcon("danger") } `
        aufstellungFound = false
      }
    } else {
      statusHtml += `Kein Verein ${_getStatusIcon("danger") }`
      aufstellungFound = false
    }
    statusHtml += "<br/>"
    // Spielklasse Auswahl required?
    if (planung.auswahl_required) {
      statusHtml += `Bitte eine Spielklasse und Halbserie auswählen ${_getStatusIcon("warning")}`
      aufstellungFound = false
    } else {
      // saison
      if ("saison" in planung && "halbserie" in planung) {
        statusHtml += `${planung.halbserie} ${planung.saison} ${_getStatusIcon("success") } `
      } else {
        statusHtml += `Keine Saison ${_getStatusIcon("danger") } `
        aufstellungFound = false
      }
      // spielklasse
      if ("spielklasse" in planung && planung.spielklasse){
        if (planung.spielklasse == current_planung.spielklasse){
          statusHtml += `${planung.spieler_spielklasse} ${_getStatusIcon("success") } `
        } else {
          statusHtml += `${planung.spieler_spielklasse} (Nicht ${current_planung.spielklasse}) ${_getStatusIcon("danger") } `
          aufstellungFound = false
        }
        
      } else {
        statusHtml += `Keine Spielklasse ${_getStatusIcon("danger") } `
        aufstellungFound = false
      }
      statusHtml += "<br/>"
      // mannschaften
      if ("mannschaften" in planung && planung.mannschaften.liste.length > 0) {
        statusHtml += `${planung.mannschaften.liste.length} Mannschaften ${_getStatusIcon("success") } `
      } else {
        statusHtml += `Keine Mannschaften ${_getStatusIcon("danger") } `
        aufstellungFound = false
      }
      // spieler
      if ("spieler" in planung && planung.spieler.liste.length > 0) {
        statusHtml += `${planung.spieler.liste.length} Spieler ${_getStatusIcon("success") } `
      } else {
        statusHtml += `Keine Spieler ${_getStatusIcon("danger") } `
        aufstellungFound = false
      }
      statusHtml += "<br/>"
      // ttrwerte 
      if ("ttrwerte" in planung && "aktuell" in planung.ttrwerte && "datestring" in planung.ttrwerte && planung.ttrwerte.datestring ) {
        statusHtml += `TTR-Stichtag: ${planung.ttrwerte.datestring} (${planung.ttrwerte.aktuell}) ${_getStatusIcon("success") } `
      } else {
        statusHtml += `Keine TTR-Werte ${_getStatusIcon("danger") } `
      }
    }

    var popoverhtml = ''
    // Only display button tooltip if a valid aufstellung has been found
    if ( aufstellungFound ) {
      popoverhtml = '<h6>Die aktuelle Planung wird verändert.</h6>'
      const attributes = ['verein', 'saison']
      attributes.forEach( attribute => {
        var loaded_value = planung[attribute]
        var current_value = current_planung[attribute]
        if ( attribute === 'saison') {
          // special case for saison+halbserie where we load a saison+halbserie but the start planning the next one with it
          var nextHalbserie = planung.halbserie == "Vorrunde" ? "Rückrunde" : "Vorrunde"
          var nextSaisonRaw = `${parseInt( planung.saison.replace("/",""),10) + 101}`
          var nextSaison = planung.halbserie == "Rückrunde" ? [nextSaisonRaw.slice(0,4),"/",nextSaisonRaw.slice(4)].join('') : planung.saison
          loaded_value = `${nextHalbserie} ${nextSaison}`
          current_value = `${current_planung.halbserie} ${current_planung.saison}`
        }
        if ( loaded_value !== current_value) { 
          popoverhtml += `<i class="fa fa-warning text-warning"></i> ${current_value} &rarr; <b>${loaded_value}</b><br/> `
        }
      })
      // TTR Werte
      const loaded_ttr_date = planung.ttrwerte.date.getTime()
      const current_ttr_date = current_planung.ttrwerte.date.getTime()
      if (current_ttr_date > 0) {
        if (loaded_ttr_date < current_ttr_date) {
          popoverhtml += `<i class="fa fa-warning text-warning"></i> TTR-Stichtag: ${current_planung.ttrwerte.datestring} &rarr; <b>${planung.ttrwerte.datestring}</b><br/> `
        } else if (loaded_ttr_date > current_ttr_date) {
          popoverhtml += `<i class="fa fa-refresh text-primary"></i> TTR-Stichtag: ${current_planung.ttrwerte.datestring} &rarr; <b>${planung.ttrwerte.datestring}</b><br/> `
        }
      } else {
        popoverhtml += `<i class="fa fa-check-circle text-success"></i> TTR-Stichtag: <b>${planung.ttrwerte.datestring}</b><br/> `
      }
      // Spieler Differenz
      const new_spieler_arr = planung.spieler.liste
      .filter( spieler => current_planung.spieler.liste
        .find( spieler1 => ( spieler1.mytt_id === spieler.mytt_id && spieler1.spielklasse === planung.spieler_spielklasse ) ) == undefined )
      if (new_spieler_arr.length > 0){
        const wird = new_spieler_arr.length == 1 ? "wird" : "werden"
        popoverhtml +=  `<i class="fa fa-plus-circle text-success"></i> <b>${new_spieler_arr.length} Spieler</b> ${wird} der aktuellen Planung hinzugefügt.<br/> `
      }
      const update_spieler_arr = current_planung.spieler.liste
      .filter( spieler => spieler.spielklasse === planung.spieler_spielklasse)
      .filter( spieler => ( planung.spieler.liste
        .find( spieler1 => ( spieler1.mytt_id === spieler.mytt_id ) ) !== undefined ) )
      if (update_spieler_arr.length > 0){
        const wird = update_spieler_arr.length == 1 ? "wird" : "werden"
        popoverhtml +=  `<i class="fa fa-refresh text-primary"></i> <b>${update_spieler_arr.length} Spieler</b> der aktuellen Planung ${wird} aktualisiert.<br/> `
      }
      const delete_spieler_arr = current_planung.spieler.liste
      .filter( spieler => spieler.spielklasse === planung.spieler_spielklasse)
      .filter( spieler => ( planung.spieler.liste
        .find( spieler1 => ( spieler1.mytt_id === spieler.mytt_id ) ) == undefined ) )
      if (delete_spieler_arr.length > 0){
        const wird = delete_spieler_arr.length == 1 ? "wird" : "werden"
        popoverhtml +=  `<i class="fa fa-minus-circle text-danger"></i> <b>${delete_spieler_arr.length} Spieler</b> der aktuellen Planung ${wird} gelöscht.<br/> `
      }
    }
    
    return { result: aufstellungFound, html: statusHtml, popoverhtml: popoverhtml }
  }

}